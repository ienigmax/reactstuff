function  Person(props) {
    return (
        <div className="person">
            <h1>{props.name}</h1>
            <p>Experience: {props.exp}</p>
        </div>
    );
}

var app = (
    <div>
        <Person name="Alex" exp="2"/>
        <Person name="Olga" exp="20"/>
    </div>
);

ReactDOM.render(app,
    document.querySelector("#p1"));
